package miguelgua.es.listalugares

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup



class ListDetailFragment : Fragment() {

    //Variables

    lateinit var list: TaskList
    lateinit var listItemsRecyclerView: RecyclerView


    companion object {
        fun newInstance(list: TaskList): ListDetailFragment {
            val fragment = ListDetailFragment()
            val argumentos = Bundle()
            argumentos.putParcelable(MainActivity.IDENTIFICADOR_INTENT, list)
            fragment.arguments = argumentos
            return  fragment
        }

    }

    /*------------------ Fin Variables ---------------------*/

    //Métodos


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        list = arguments!!.getParcelable(MainActivity.IDENTIFICADOR_INTENT)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        //Inflar el Layout
         val view = inflater.inflate(R.layout.fragment_list_detail, container, false)
        view.let {
            //Comprobar Layout
            listItemsRecyclerView               = it.findViewById(R.id.list_items_recyclerView)
            listItemsRecyclerView.adapter       = ListItemsRecyclerViewAdapter(list)
            listItemsRecyclerView.layoutManager = LinearLayoutManager(activity)

        }

        return view
    }

    //Método addTask
    fun addTask(item: String) {
        list.tareas.add(item)
        val listRecyclerAdapter  = listItemsRecyclerView.adapter as ListItemsRecyclerViewAdapter
        listRecyclerAdapter.list = list
        listRecyclerAdapter.notifyDataSetChanged()
    }

    /*------------------ Fin Métodos ---------------------*/


}
