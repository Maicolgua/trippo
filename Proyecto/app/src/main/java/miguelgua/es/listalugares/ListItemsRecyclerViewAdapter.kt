package miguelgua.es.listalugares

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

class ListItemsRecyclerViewAdapter(var list: TaskList): RecyclerView.Adapter<ListItemViewHolder>() {

    //Métodos


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListItemViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.task_view_holder , p0, false)
        return ListItemViewHolder(view)
    }


    override fun getItemCount(): Int {
        return list.tareas.size
    }


    override fun onBindViewHolder(p0: ListItemViewHolder, p1: Int) {
        if (p0 != null) {
            p0.taskTextView.text = list.tareas[p1]
        }
    }

    /*------------------ Fin Métodos ---------------------*/
}