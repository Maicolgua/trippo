package miguelgua.es.listalugares

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.widget.EditText

class ListDetailActivity : AppCompatActivity() {

    //Variables

    //Referencia al TaskList
    lateinit var list:TaskList

    //Lista de actividades
    lateinit var listItemsRecyclerView: RecyclerView
    //Botón
    lateinit var addTaskButton: FloatingActionButton

    /*------------------ Fin Variables ---------------------*/

    //Métodos


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detail)

        list  = intent.getParcelableExtra(MainActivity.IDENTIFICADOR_INTENT)
        title = list.nombre

        //Nueva Lista de Items

        listItemsRecyclerView               = findViewById(R.id.detailList_recyclerView)
        //Definir el adapter
        listItemsRecyclerView.adapter       = ListItemsRecyclerViewAdapter(list)
        //Definir el Layout Manager
        listItemsRecyclerView.layoutManager = LinearLayoutManager(this)

        //Botón
        addTaskButton = findViewById(R.id.add_task_fab)
        addTaskButton.setOnClickListener {
            showCreateDialog()
        }

    }

    //Método showCreateDialog

    fun showCreateDialog(){
        val taskEditText = EditText(this)
        taskEditText.inputType = InputType.TYPE_CLASS_TEXT

        //Alert Dialog
        AlertDialog.Builder(this)
            .setTitle("Añadir Actividad")
            .setView(taskEditText)
            .setPositiveButton("Añadir") {
                    dialog, error ->
                val actividad = taskEditText.text.toString()
                list.tareas.add(actividad)

                val recyclerAdapter = listItemsRecyclerView.adapter as ListItemsRecyclerViewAdapter
                recyclerAdapter.notifyItemInserted(list.tareas.size)
                dialog.dismiss()

            }.create()
            .show()
    }


    override fun onBackPressed() {

        val bundle = Bundle()
        bundle.putParcelable(MainActivity.IDENTIFICADOR_INTENT, list)

        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)

        super.onBackPressed()
    }

    /*------------------ Fin Métodos ---------------------*/

}
