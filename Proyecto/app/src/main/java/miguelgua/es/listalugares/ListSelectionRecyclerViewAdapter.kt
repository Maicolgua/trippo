package miguelgua.es.listalugares

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

class ListSelectionRecyclerViewAdapter(val lists: ArrayList<TaskList>,
                                       val clickListener: ListSelectionRecyclerViewListener):
    RecyclerView.Adapter<ListSelectionViewHolder>() {


    //Métodos

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListSelectionViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.list_selection_view_holder, p0, false)

        return ListSelectionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(p0: ListSelectionViewHolder, p1: Int) {
        if(p0 != null){
            p0.listPosition.text = (p1 + 1).toString()
            p0.listtitle.text    = lists.get(p1).nombre
            p0.itemView.setOnClickListener {
                clickListener.listItemSeleccionado(lists.get(p1))
            }
        }
    }

    //Método para añadir lista
    fun addList(list: TaskList){
        lists.add(list)
        notifyDataSetChanged()
    }

    //Crear Interfaz
    interface ListSelectionRecyclerViewListener {
        fun listItemSeleccionado(list: TaskList)
    }

    /*------------------ Fin Métodos ---------------------*/
}