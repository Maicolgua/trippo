package miguelgua.es.listalugares

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*



class HomeActivity : AppCompatActivity() {

    //Método onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)





        boton_Notas.setOnClickListener {
            //Intent Itinerario
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        //Barra de Navegación

        boton_Mapa.setOnClickListener {
            //Intent Lugares Cercanos
            val intentMap = Intent(this, MapsActivity::class.java)
            startActivity(intentMap)
        }

        boton_Camara.setOnClickListener {
            //Intent Cámara
            val intentCamera = Intent(this, CameraActivity::class.java)
            startActivity(intentCamera)
        }


    }


    /*------------------ Fin Método onCreate ---------------------*/
}
