package miguelgua.es.listalugares

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class ListItemViewHolder (itemView: View): RecyclerView.ViewHolder(itemView) {

    //Variables
    val taskTextView = itemView.findViewById<TextView>(R.id.textView_task)
}