package miguelgua.es.listalugares

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class ListDataManager(val contexto: Context) {

    //Métodos

    //Método Guardar Lista
    fun guardarLista(list: TaskList) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(contexto).edit()
        sharedPreferences.putStringSet(list.nombre, list.tareas.toHashSet())
        sharedPreferences.apply()
    }

    //Método recuperar Datos
    fun leerListasGuardadas(): ArrayList<TaskList> {
        val sharedPreferences          = PreferenceManager.getDefaultSharedPreferences(contexto)
        val sharedPreferencesContenido = sharedPreferences.all
        val taskList                   = ArrayList<TaskList>()

        for (listaGuardada in sharedPreferencesContenido) {
            val itemHashSet = listaGuardada.value as HashSet<String>
            val lista       = TaskList(listaGuardada.key, ArrayList(itemHashSet))
            taskList.add(lista)
        }
        return taskList
    }

    /*------------------ Fin Métodos ---------------------*/
}