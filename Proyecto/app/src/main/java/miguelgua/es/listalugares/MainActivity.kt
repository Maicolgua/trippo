package miguelgua.es.listalugares

import android.app.ActionBar
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.FrameLayout

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), ListSelectionFragment.OnListFragmentInteractionListener {

    //Variables

    //objeto
    companion object {
        val IDENTIFICADOR_INTENT = "Listados"
        val LIST_DETAIL_REQUEST_CODE = 123
    }

    private  var listSelectionFragment           = ListSelectionFragment.newInstance()
    private  var fragmentContainer: FrameLayout? = null



    //Share Preferences

    val ID_INTENT = "Listados"

    /*------------------ Fin Variables ---------------------*/

    //Métodos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //Leer listas guardadas
        fab.setOnClickListener { view ->
            mostrarListaDialog()
        }
        //Referencia Fragment
       fragmentContainer = findViewById(R.id.fragment_container)
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, listSelectionFragment)
            .commit()


    }

    //Método Mostrar Dialogo
    private fun mostrarListaDialog(){
        val tituloDialog = getString(R.string.titulo_dialog)
        val textoBoton   = getString(R.string.crear_lista)

        //Dialog
        val builder                 = AlertDialog.Builder(this)
        val listTitleEditText       = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(tituloDialog)
        builder.setView(listTitleEditText)
        builder.setPositiveButton(textoBoton) {
            dialog, _ ->
            val list = TaskList(listTitleEditText.text.toString())
            listSelectionFragment.addList(list)
            dialog.dismiss()
            mostrarListaDetalle(list)

        }
        builder.create().show()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*
        return when (item.itemId) {

            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)

        }
        */
        return false
    }

    //Método para navegar a la siguiente pantalla
    private  fun mostrarListaDetalle(list: TaskList){
        val listDetailIntent = Intent(this, ListDetailActivity::class.java)
        listDetailIntent.putExtra(IDENTIFICADOR_INTENT, list)
        //startActivity(listDetailIntent)
        startActivityForResult(listDetailIntent, LIST_DETAIL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LIST_DETAIL_REQUEST_CODE){
            data?.let {
                listSelectionFragment.saveList(data.getParcelableExtra<TaskList>(ID_INTENT))
            }
        }
    }

    override fun onListItemClicked(list: TaskList) {
        mostrarListaDetalle(list)
    }

    /*------------------ Fin Métodos ---------------------*/
}
