package miguelgua.es.listalugares

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ListSelectionFragment : Fragment(), ListSelectionRecyclerViewAdapter.ListSelectionRecyclerViewListener {


    //Variables

    private var listener          : OnListFragmentInteractionListener? = null
    lateinit var listRecyclerView : RecyclerView
    lateinit var listaDataManager : ListDataManager

    interface OnListFragmentInteractionListener {
        fun onListItemClicked(list: TaskList)
    }

    companion object {

        fun newInstance(): ListSelectionFragment {
            val fragment = ListSelectionFragment()
        return fragment

                }
            }

    /*------------------ Fin Variables ---------------------*/

    //Métodos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_selection, container, false)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
            listaDataManager = ListDataManager(context)
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var lists = listaDataManager.leerListasGuardadas()
        view?.let {
            listRecyclerView = it.findViewById(R.id.lists_recyclerView)
            listRecyclerView.layoutManager = LinearLayoutManager(activity)
            listRecyclerView.adapter = ListSelectionRecyclerViewAdapter(lists, this )

        }
    }

    override fun listItemSeleccionado(list: TaskList) {
        listener?.onListItemClicked(list)
    }

    fun addList(list: TaskList){
        listaDataManager.guardarLista(list)
        val recyclerAdapter = listRecyclerView.adapter as ListSelectionRecyclerViewAdapter
        recyclerAdapter.addList(list)

    }

    fun saveList(list: TaskList) {
        listaDataManager.guardarLista(list)
        updateLists()

    }

    private fun updateLists(){
        val lists = listaDataManager.leerListasGuardadas()
        listRecyclerView.adapter = ListSelectionRecyclerViewAdapter(lists, this)
    }

    /*------------------ Fin Métodos ---------------------*/

}
