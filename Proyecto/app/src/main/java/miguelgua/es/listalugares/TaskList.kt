package miguelgua.es.listalugares

import android.os.Parcel
import android.os.Parcelable
import android.os.WorkSource

class TaskList constructor(val nombre: String, val tareas: ArrayList<String> =  ArrayList<String>()): Parcelable {


    //Constructor
    constructor(source: Parcel): this(
        source.readString(),
        source.createStringArrayList()
    )

    /*------------------ Fin Constructor ---------------------*/

    //Métodos
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(nombre)
        dest.writeStringList(tareas)
    }

    override fun describeContents(): Int {
        return  0
    }

    /*------------------ Fin Métodos ---------------------*/

    //Crear objeto
    companion object CREATOR: Parcelable.Creator<TaskList> {

        override fun createFromParcel(source: Parcel): TaskList {
            return TaskList(source)
        }

        override fun newArray(size: Int): Array<TaskList?> {
            return arrayOfNulls(size)
        }

    }
    /*------------------ Fin Objeto ---------------------*/
}