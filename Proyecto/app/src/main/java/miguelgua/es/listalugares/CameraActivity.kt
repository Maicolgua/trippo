package miguelgua.es.listalugares

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_camera.*

class CameraActivity : AppCompatActivity() {

    //Variables

    private val PERMISSION_CODE    = 100
    private val IMAGE_CAPTURE_CODE = 101
    var image_uri: Uri? = null

    /*------------------ Fin Variables ---------------------*/

    //Métodos


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        //Método Botón
        capture_btn.setOnClickListener {
            //Permiso
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED ||
                        checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED){
                    //El permiso no fue habilitado
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //Mostrar popup para requerir permiso
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else {
                    //Permiso  habilitado
                    openCamera ()
                }

            }
            else {
                //Sistema OS es < Marshmallow
                openCamera()
            }
        }

        //Barra de Navegación

        botonIti.setOnClickListener {
            //Intent Itinerario
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        botonMap.setOnClickListener {
            //Intent Lugares Cercanos
            val intentMap = Intent(this, MapsActivity::class.java)
            startActivity(intentMap)
        }



        /*------------------ Fin Barra de Navegación ---------------------*/

    }

    //Método openCamera
    private  fun openCamera(){
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE,"Nueva Imagen")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Desde la cámara")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        //Camera Intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)

    }
    //Permisos
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    //Permisos del popup fueron aprobados
                    openCamera()
                }
                else {
                    //Permisos del popup fueron denegados
                    Toast.makeText(this, "Permisos Denegados", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    //Método onActivityResult
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //Llamado cuando la imagen fue capturada del camera intent
        if (resultCode == Activity.RESULT_OK) {
            //Configurar la imagen capturada al imageView
            image_view.setImageURI(image_uri)

        }
    }

    /*------------------ Fin Métodos ---------------------*/
}
